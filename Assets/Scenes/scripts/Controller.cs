﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public float allSpeed = 5f;
    float speedX;
    float speedY;
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

   
    void FixedUpdate()
    {
        
        if (Input.GetKey(KeyCode.D))
        {
            speedX = allSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            speedX = -allSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.W))
        {
            speedY = allSpeed*Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            speedY = -allSpeed * Time.deltaTime;
        }
        transform.Translate(new Vector3(speedX, speedY, 0));
        speedX = 0;
        speedY = 0;

    }
}
